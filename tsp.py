'''
    tsp.py
    Contains Class TSP definition,
    TSP problem parser and
    has access to the tsp problem definitions
'''
import os
import numpy as np

EXPLICIT = "EXPLICIT"
CEIL_2D = "CEIL_2D"
EUC_2D = "EUC_2D"
EUC_3D = "EUC_3D"

UPPER_DIAG_ROW = "UPPER_DIAG_ROW"
LOWER_DIAG_ROW = "LOWER_DIAG_ROW"
UPPER_ROW = "UPPER_ROW"
LOWER_ROW = "LOWER_ROW"


class TSP:
    def __init__(self, name, comments, dimension, edge_w_type, path):
        self.name = name
        self.comments = comments
        self.dimension = dimension
        self.edge_w_type = edge_w_type
        self.path = path

    def dist(self, a, b):
        pass

    def __str__(self):
        return f"{self.name}\nDIM: {self.dimension} EDGE_TYPE: {self.edge_w_type}\nCOMMENTS: {self.comments}"

    def __repr__(self):
        return f"{self.name} ({self.dimension}) {{{self.edge_w_type}}}"

    def __lt__(self, other):
        return self.dimension < other.dimension


class TSPExplicit(TSP):
    def __init__(self, matrix, *args):
        super().__init__(*args)
        self.matrix = matrix

    def set_matrix(self, matrix):
        self.matrix = matrix

    def dist(self, a, b):
        return self.matrix[a-1][b-1]


class TSPCoordsEuc(TSP):
    def __init__(self, nodes, *args, ceil=False):
        super().__init__(*args)
        self.nodes = nodes
        self.ceil = ceil

    def set_nodes(self, nodes):
        self.nodes = nodes

    def dist(self, a, b):
        res = np.linalg.norm(self.nodes[a] - self.nodes[b])
        if self.ceil:
            return np.ceil(res)
        else:
            return res


def parse_head(f):
    with open(f, 'r') as fr:
        comments = []
        name = ""
        dimension = -1
        edge_w_type = None

        for line in fr.readlines():
            if line.find("EDGE_WEIGHT_TYPE") >= 0:
                edge_w_type = line.split()[-1]
                break
            if line.find("TYPE") >= 0 and not line.find("TSP") >= 0:
                raise Exception("TYPE must be TSP")
            elif line.find("NAME") >= 0:
                name = line.split()[-1]
            elif line.find("COMMENT") >= 0:
                comments.append(line[line.index(":")+2:].strip())
            elif line.find("DIMENSION") >= 0:
                dimension = int(line.split()[-1])

    if edge_w_type == EXPLICIT:
        return TSPExplicit(None, name, comments, dimension, edge_w_type, f)
    elif edge_w_type == CEIL_2D:
        return TSPCoordsEuc(None, name, comments, dimension, edge_w_type, f, ceil=True)
    elif edge_w_type == EUC_2D or edge_w_type == EUC_3D:
        return TSPCoordsEuc(None, name, comments, dimension, edge_w_type, f)
    else:
        raise Exception(f"Weight type {edge_w_type} not implemented")


def get_available_TSP():
    files = []
    directory = "./problems"
    for f in os.listdir(directory):
        if os.path.isfile(os.path.join(directory,f)):
            files.append(os.path.join(directory,f))

    res = []
    for f in files:
        res.append(parse_head(f))
    return res

def load_full_problem(problem):
    if type(problem) == str:
        problem = parse_head(problem)

    with open(problem.path, 'r') as fr:
        if problem.edge_w_type == EXPLICIT:
            line = fr.readline()
            while line.find("EDGE_WEIGHT_FORMAT") < 0:
                line = fr.readline()
            w_format = line.split()[-1]

            while line.find("EDGE_WEIGHT_SECTION") < 0:
                line = fr.readline()

            if w_format == UPPER_DIAG_ROW:
                problem.set_matrix(parse_upper_diag(fr, problem.dimension))
            elif w_format == UPPER_ROW:
                problem.set_matrix(parse_upper(fr, problem.dimension))
            elif w_format == LOWER_DIAG_ROW:
                problem.set_matrix(parse_lower_diag(fr, problem.dimension))
            elif w_format == LOWER_ROW:
                problem.set_matrix(parse_lower(fr, problem.dimension))
            else:
                raise Exception(f"EDGE_WEIGHT_FORMAT {w_format} not supported")

        elif problem.edge_w_type in [CEIL_2D, EUC_2D, EUC_3D]:
            line = fr.readline()
            while line.find("NODE_COORD_SECTION") < 0:
                line = fr.readline()

            if problem.edge_w_type == EUC_3D:
                problem.set_nodes(parse_nodes(fr, problem.dimension, 3))
            else:
                problem.set_nodes(parse_nodes(fr, problem.dimension, 2))

        else:
            raise Exception(f"Unsupported edge_type {problem.edge_w_type}")

    return problem

def parse_lower_diag(fr, dim):
    res = np.ndarray((dim, dim), dtype=int)
    x, y = 0, 0
    for line in fr.readlines():
        if y >= dim:
            break
        for d in line.split():
            res[x,y] = int(d)
            res[y,x] = int(d)
            if x >= y:
                y += 1
                x = 0
            else:
                x += 1
    return res


def parse_lower(fr, dim):
    res = np.ndarray((dim, dim), dtype=int)
    x, y = 0, 1
    res[0,0] = 0
    for line in fr.readlines():
        if y >= dim:
            break
        for d in line.split():
            res[x,y] = int(d)
            res[y,x] = int(d)
            x += 1
            if x == y:
                res[x,y] = 0
                y += 1
                x = 0
    return res

def parse_upper_diag(fr, dim):
    res = np.ndarray((dim, dim), dtype=int)
    x, y = 0, 0
    for line in fr.readlines():
        if y >= dim:
            break
        for d in line.split():
            res[x,y] = int(d)
            res[y,x] = int(d)
            x += 1
            if x >= dim:
                y += 1
                x = y
    return res

def parse_upper(fr, dim):
    res = np.ndarray((dim, dim), dtype=int)
    x, y = 1, 0
    res[0,0] = 0
    for line in fr.readlines():
        if x >= dim:
            break
        for d in line.split():
            res[x,y] = int(d)
            res[y,x] = int(d)
            x += 1
            if x >= dim:
                y += 1
                x = y+1
                res[y,y] = 0
    return res

def parse_nodes(fr, dim, dim2):
    res = np.ndarray((dim+1, dim2), dtype=float)
    res[0] = np.array([np.NaN]*dim2)
    for _ in range(dim):
        line = fr.readline()
        node, *coords = line.split()
        res[int(node)] = np.array([float(i) for i in coords])
    return res