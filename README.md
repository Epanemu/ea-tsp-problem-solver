# Using EA to optimize symetric TSP
## About
Implementation of multiple Local search and Evolutionary algorithms with the goal to optimize solutions to the Traveling salesperson problems from [TSPLIB](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/)

## Running
1) Unzip the results.zip into `./results` folder to enable visualization of precomputed data.

2) Open Jupyter notebook `./walkthrough_report.ipynb` and follow the instructions inside.

If you wish to run the algorithms on a different problem, first add the problem definition file in the `./problems` folder at the top level. This way it wil be recognized using the parser function.

**NOTICE**: Not all tsp definition file types are supported by the parser. Problems with `EXACT` and `EUCLIDEAN` distance should work fine.

## Requirements
- `numpy`
### For visualizations
- `matplotlib` (for plots)
- `ipywidgets` (for interactive plots)
- `pandas` (for tables)
