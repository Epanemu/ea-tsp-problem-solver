import numpy as np
from local_search import Perturbation, PlaceSwap
from tsp import TSP
from typing import Optional

class Initialization:
    @staticmethod
    def init(pop_size, tsp, rng):
        pass

class RndInitialization(Initialization):
    @staticmethod
    def init(pop_size, tsp, rng):
        return np.array([rng.permutation(range(1, tsp.dimension+1)) for _ in range(pop_size)], dtype=int)


class Population:
    def __init__(self, tsp, rng, pop_size=100, initialization: Optional[Initialization] = RndInitialization):
        self.tsp = tsp
        if pop_size % 2 == 1:
            pop_size += 1
        self.pop_size = pop_size
        if initialization is not None:
            self.data = initialization.init(pop_size, tsp, rng)

    def eval_one(self, perm):
        d = self.tsp.dist(perm[0], perm[-1])
        for i in range(1, len(perm)):
            d += self.tsp.dist(perm[i-1], perm[i])
        return d

    def evaluate(self):
        '''Returns number of calls of evaluation function'''
        self.fitness = np.array([self.eval_one(perm) for perm in self.data], dtype=int)
        return self.pop_size

    def best_f(self):
        return np.min(self.fitness)

    def best(self):
        return self.data[np.argmin(self.fitness)]

    def __str__(self):
        return f"Population size: {self.pop_size}\n{self.data[:5]} ..."


class CrossOver:
    @staticmethod
    def xover(p1, p2, rng):
        pass

class OrderXover(CrossOver):
    @staticmethod
    def xover(p1, p2, rng):
        i_start = rng.integers(len(p1))
        seg_len = rng.integers(len(p1))

        c1 = np.empty_like(p1)
        order = [p2[(i_start+seg_len+i) % len(p2)] for i in range(len(p2))]
        # print(order)
        for i in range(len(p1)):
            i_mod = (i_start+i) % len(p1)
            if i < seg_len:
                c1[i_mod] = p1[i_mod]
                # print(p1[i_mod], p1, order, i_mod)
                order.remove(p1[i_mod])
            else:
                c1[i_mod] = order[i - seg_len]

        c2 = np.empty_like(p2)
        order = [p1[(i_start+i) % len(p1)] for i in range(len(p1))]
        for i in range(len(p2)):
            i_mod = (i_start+i) % len(p2)
            if i < seg_len:
                c2[i_mod] = p2[i_mod]
                order.remove(p2[i_mod])
            else:
                c2[i_mod] = order[i - seg_len]

        return c1, c2


class AltEdgesXover(CrossOver):
    @staticmethod
    def xover(p1, p2, rng):
        next1 = [np.nan]*(len(p1)+1)
        next2 = [np.nan]*(len(p1)+1)
        for i in range(1,len(p1)):
            next1[p1[i-1]] = p1[i]
            next2[p2[i-1]] = p2[i]
        next1[p1[-1]] = p1[0]
        next2[p2[-1]] = p2[0]

        c1 = AltEdgesXover.half(p1[0], next2, next1, list(range(1, len(p1)+1)), rng)
        c2 = AltEdgesXover.half(p2[0], next1, next1, list(range(1, len(p1)+1)), rng)

        return c1, c2

    @staticmethod
    def half(start, next1, next2, values, rng):
        child = np.ndarray((len(values),), dtype=int)
        first = True
        child[0] = start
        values.remove(start)
        i = 1
        while len(values) > 0:
            if first:
                if next1[child[i-1]] in values:
                    child[i] = next1[child[i-1]]
                    first = False
                else:
                    child[i] = rng.choice(values)
            else:
                if next2[child[i-1]] in values:
                    child[i] = next2[child[i-1]]
                    first = True
                else:
                    child[i] = rng.choice(values)

            values.remove(child[i])
            i += 1
        return child


class Selection:
    @staticmethod
    def select(n, pop: Population, rng):
        pass

class StochasticUniversalSampling(Selection):
    @staticmethod
    def select(n, pop: Population, rng):
        max_f = np.max(pop.fitness)
        f_rev = -pop.fitness + (max_f*1.1)
        sum_f = np.sum(f_rev)
        elem_angles = f_rev / sum_f

        angle_d = 1/n
        angle = rng.random()
        while angle-angle_d > 0:
            angle -= angle_d

        chosen_indices = []
        i = 0
        elem_angle = 0
        for _ in range(n):
            while elem_angle+elem_angles[i] < angle:
                elem_angle += elem_angles[i]
                i += 1

            chosen_indices.append(i)
            angle += angle_d
        return np.array(chosen_indices)


class RankSelection(Selection):
    @staticmethod
    def select(n, pop: Population, rng):
        return np.argsort(pop.fitness)[:n]


class ReplacementStrategy:
    @staticmethod
    def select(old: Population, new: Population, rng):
        pass

class TopHalvesStrategy(ReplacementStrategy):
    @staticmethod
    def select(old: Population, new: Population, rng):
        new_i = np.argsort(new.fitness)
        old_i = np.argsort(old.fitness)
        half = int(old_i.size / 2)
        result = Population(old.tsp, rng, old.pop_size, initialization=None)
        result.data = np.append(old.data[old_i[:half]], new.data[new_i[:half]], axis=0)
        result.fitness = np.append(old.fitness[old_i[:half]], new.fitness[new_i[:half]], axis=0)
        return result


class SteadyStateStrategy(ReplacementStrategy):
    @staticmethod
    def select(old: Population, new: Population, rng):
        return new


class Evolution:
    def __init__(self,
                 tsp: TSP,
                 pop_size = 100,
                 initialization: Initialization = RndInitialization,
                 selection: Selection = StochasticUniversalSampling,
                 xover: CrossOver = AltEdgesXover,
                 mutation: Perturbation = PlaceSwap,
                 replacement: ReplacementStrategy = TopHalvesStrategy,
                 rng = np.random.default_rng(),
                 xover_prob=1,
                 mutation_prob=0.5):
        self.tsp = tsp
        self.pop_size = pop_size
        self.initialization = initialization
        self.selection = selection
        self.xover = xover
        self.mutation = mutation
        self.replacement = replacement
        self.rng = rng
        self.xover_prob = xover_prob
        self.mutation_prob = mutation_prob

        self.evals = 0
        self.history = []

    def select_parents(self, population: Population):
        indices = self.selection.select(self.pop_size, population, self.rng)
        parents = Population(self.tsp, self.rng, self.pop_size, initialization=None)
        parents.data = population.data[indices]
        parents.fitness = population.fitness[indices]
        return parents

    def crossover(self, parents: Population):
        children = Population(self.tsp, self.rng, parents.pop_size, initialization=None)
        children.data = np.empty_like(parents.data)
        for i in range(1, len(parents.data), 2):
            if self.rng.random() <= self.xover_prob:
                children.data[i-1], children.data[i] = self.xover.xover(parents.data[i-1], parents.data[i], self.rng)
            else:
                children.data[i-1], children.data[i] = parents.data[i-1], parents.data[i]
        return children

    def mutate(self, children: Population):
        result = Population(self.tsp, self.rng, children.pop_size, initialization=None)
        result.data = np.apply_along_axis(lambda v: v if self.rng.random() > self.mutation_prob else self.mutation.perturbate(v, self.rng),
                                          1,
                                          children.data)
        return result

    def run(self, n_evaluations):
        goal_evals = self.evals + n_evaluations

        old = Population(self.tsp, self.rng, pop_size=self.pop_size, initialization=self.initialization)
        self.evals += old.evaluate()
        self.history.append((self.evals, old.best_f(), old.best()))
        while self.evals < goal_evals:
            parents = self.select_parents(old)
            children = self.crossover(parents)
            new = self.mutate(children)
            self.evals += new.evaluate()
            old = self.replacement.select(old, new, self.rng)
            if old.best_f() < self.history[-1][1]:
                self.history.append((self.evals, old.best_f(), old.best()))
        return self.history[-1]