import numpy as np
import itertools
from tsp import TSP

class Solution:
    '''
        Solution is a permutation of elements from 1 to tsp.dimension+1
        It knows it's total distance
    '''

    def __init__(self, perm, tsp):
        self.perm = np.array(perm)
        self.dist = tsp.dist(perm[0], perm[-1])
        for i in range(1, len(perm)):
            self.dist += tsp.dist(perm[i-1], perm[i])

    def __str__(self):
        return f"{self.dist}: {self.perm}"


def random_solution(tsp: TSP, rng):
    return Solution(rng.permutation(range(1, tsp.dimension+1)), tsp)

class Perturbation:
    @staticmethod
    def perturbate(perm, rng):
        pass

class PlaceSwap(Perturbation):
    @staticmethod
    def perturbate(perm, rng):
        '''Swaps 2 random positions in the permutation'''
        i1 = rng.integers(len(perm))
        i2 = rng.integers(len(perm))
        p = perm.copy()
        p[i1], p[i2] = p[i2], p[i1]
        return p

class PosMove(Perturbation):
    @staticmethod
    def perturbate(perm, rng):
        '''Moves 1 random element to a different position in the permutation'''
        e_pos = rng.integers(len(perm))
        pos = rng.integers(len(perm))
        p = np.append(perm[:e_pos],perm[e_pos+1:])
        return np.insert(p, pos, perm[e_pos])

class SeqFlip(Perturbation):
    @staticmethod
    def perturbate(perm, rng):
        '''Flips a random subsequence'''
        i1 = rng.integers(len(perm))
        i2 = rng.integers(len(perm))
        if i1 > i2:
            i1, i2 = i2, i1
        p = np.append(perm[:i1], np.append(np.flip(perm[i1:i2+1], 0), perm[i2+1:]))
        return p

class LocalSearch:
    STUCK_LIMIT = 500

    def __init__(self, tsp: TSP, init_f, pert: Perturbation, rng):
        self.tsp = tsp
        self.best = np.inf
        self.init_f = init_f
        self.pert = pert
        self.rng = rng

        self.evals = 0
        self.history = []

    def run(self, n_evaluations, restarted=False):
        '''
            Run the set up local search for n evaluations of fitness function
        '''
        goal_evals = self.evals+n_evaluations
        self.best = self.init_f(self.tsp, self.rng)
        self.evals += 1
        self.history.append((self.evals, self.best.dist, self.best.perm))
        while self.evals < goal_evals:
            curr = Solution(self.pert.perturbate(self.best.perm, self.rng), self.tsp)
            self.evals += 1
            if curr.dist < self.best.dist:
                self.best = curr
                self.history.append((self.evals, self.best.dist, self.best.perm))

            if restarted and self.evals - self.history[-1][0] > self.STUCK_LIMIT:
                return self.evals - self.history[-1][0]
        return self.best

    def restarted_run(self, n_evaluations):
        goal_evals = self.evals + n_evaluations
        while self.evals < goal_evals:
            self.run(goal_evals - self.evals, restarted=True)

        history = []
        lowest = np.inf
        for h in self.history:
            if h[1] < lowest:
                lowest = h[1]
                history.append(h)
        self.history = history
        self.best.perm = self.history[-1][2]
        self.best.dist = self.history[-1][1]
        return self.best

def complete_search(tsp: TSP):
    evals = 0
    best = (np.inf, [])
    history = []
    for permutation in itertools.permutations(range(2, tsp.dimension+1)):
        sol = Solution([1,*permutation], tsp)
        evals += 1
        if sol.dist < best[0]:
            best = (sol.dist, sol.perm)
            history.append((evals, sol.dist, sol.perm))
    return evals, history

def constructional_heuristic(tsp: TSP, start):
    '''Simple greedy heuristic, starts from 1, takes allways the closest node as next in permutation'''
    dist_calls = 0
    solution = [start]
    solution_eval = 0
    for _ in range(tsp.dimension - 1):
        min_d = np.inf
        min_i = None
        for i in range(1, tsp.dimension+1):
            if i not in solution:
                d = tsp.dist(solution[-1], i)
                dist_calls += 1
                if d < min_d:
                    min_d = d
                    min_i = i
        if min_i is None:
            raise Exception("No unexplored node left")
        solution.append(min_i)
        solution_eval += min_d
    solution_eval += tsp.dist(solution[-1], solution[0])
    dist_calls += 1
    return (dist_calls, solution_eval, solution)